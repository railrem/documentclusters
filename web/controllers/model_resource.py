import falcon
import logging
import time

class ModelResource(object):
    def __init__(self, app):
        self.app = app
        self.logger = logging.getLogger('build_modelapp.' + __name__)

    def on_get(self, req, resp):

        start_time = time.time()

        try:
            self.app.buildModel()
        except Exception as ex:
            self.logger.error(ex)

            description = ('Database not available')

            raise falcon.HTTPServiceUnavailable(
                'Service Outage',
                description,
                30)
        finish_time = time.time() - start_time
        # An alternative way of doing DRY serialization would be to
        # create a custom class that inherits from falcon.Request. This
        # class could, for example, have an additional 'doc' property
        # that would serialize to JSON under the covers.
        req.context['result'] = round(finish_time)

        resp.status = falcon.HTTP_200

