import logging
import core.db
import falcon


class TestResource(object):
    def __init__(self, app):
        self.app = app
        self.logger = logging.getLogger('testapp.' + __name__)

    def on_get(self, req, resp):

        body = req.get_param('body')
        document = core.db.Document(body=body)
        document.title='empty'
        document.all = 'empty'
        document.desicion='empty'
        document.judge = 'empty'
        document.info = 'empty'
        document.path='tr'
        document.save()
        try:
            result = self.app.getAnalogs(document.id)
        except KeyError:
            raise falcon.HTTPBadRequest(
                'Missing thing',
                'A thing must be submitted in the request body.')

        req.context['result'] = result
        resp.status = falcon.HTTP_200
