import logging
import falcon

class AnalogsResource(object):

    def __init__(self, app):
        self.app = app
        self.logger = logging.getLogger('analogsapp.' + __name__)

    def on_get(self, req, resp):
        document_id = req.get_param('document_id')
        try:
            result = self.app.getAnalogs(document_id)
        except Exception as ex:
            self.logger.error(ex)

            description = ('Database not available')

            raise falcon.HTTPServiceUnavailable(
                'Service Outage',
                description,
                30)

        # An alternative way of doing DRY serialization would be to
        # create a custom class that inherits from falcon.Request. This
        # class could, for example, have an additional 'doc' property
        # that would serialize to JSON under the covers.
        req.context['result'] = result

        resp.status = falcon.HTTP_200