Кластеризация текстовых документов

### Установка ###

* Скачать архив дел https://drive.google.com/drive/folders/0B3LVNy68q4mkcTZad2tNLWNnSTg
содержимое папки result перенести в папку data_storage
* выполнить команду python prepare.py
* установить сервер gunicorn
* запустить команду
gunicorn engine:app -c [папка проекта]/config.py


### Доступные методы API ###

* localhost:9005/rebuild перестроение модели

* localhost:9005/analogs?document_id={Id документа}

* localhost:9005/test?text={текст}
