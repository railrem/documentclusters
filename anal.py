import core.app
import time
from core.db import Document

if __name__ == "__main__":
    nums = [2500,2250,1750,1500,1250,1000,750,500,250,100]
    nums = [100]
    for num in nums:
        documents = Document.select().where(Document.body.is_null(False) & (Document.id.not_in([19366]))) \
            .limit(num)
        print('***len***')
        print(len(documents))
        print('***len***')
        start_time = time.time()
        # tool = core.tools.Tools()
        # tool.fill_database('data_storage/acts')
        app = core.app.App()
        app.buildPointModel(documents)
        # print(app.getAnalogs(19366,10))
        finish_time = time.time() - start_time
        print('time: %s s' % round(finish_time))