import nltk
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
import re
import math
from sklearn.cluster import KMeans, SpectralClustering
from core.serializetool import *
from core.db import *
from sklearn.feature_extraction.text import CountVectorizer
import scipy as sp


class DocumentClustering:
    TYPE_KMEANS = 'kmeans'
    TYPE_SPECTRAL = 'spectral'

    def __init__(self):

        self.stemmer = SnowballStemmer("russian")
        self.tfidf_vectorizer = TfidfVectorizer(tokenizer=self.tokenize_and_stem,
                                                max_df=0.8,
                                                min_df=0.2)
        self.stop_words = nltk.corpus.stopwords.words('russian')
        self.count_vectorizer = CountVectorizer(
            tokenizer=self.tokenize_and_stem,
            stop_words=self.stop_words,
        )

    def buildKmeansModel(self, documents):
        document_bodies = [document.body for document in documents]
        tfidf_matrix = self.tfidf_vectorizer.fit_transform(document_bodies)
        cluster_count = math.floor(math.log2(len(documents)))
        km = KMeans(n_clusters=cluster_count)
        km = km.fit(tfidf_matrix)
        SerializeTool.serializeModel(km)
        SerializeTool.serializeClusteringIndex(km, tfidf_matrix, self.tfidf_vectorizer,
                                               documents, DocumentClustering.TYPE_KMEANS)

    def buildSpectralModel(self, documents):
        document_bodies = [document.body for document in documents]
        tfidf_matrix = self.tfidf_vectorizer.fit_transform(document_bodies)
        cluster_count = math.floor(math.log2(len(documents)))
        km = SpectralClustering(n_clusters=cluster_count)
        km = km.fit(tfidf_matrix)
        SerializeTool.serializeModel(km)
        SerializeTool.serializeClusteringIndex(km, tfidf_matrix, self.tfidf_vectorizer,
                                                 documents, DocumentClustering.TYPE_SPECTRAL)

    def getAnalogs(self, document_id, count, type_id):
        document = Document.select().where(Document.id == document_id).get()
        km = SerializeTool.deserializeModel(type_id)
        serialize_model = SerializeTool.deserializeClusteringIndex(type_id)
        self.tfidf_vectorizer.set_params(vocabulary=serialize_model['vocabulary'])
        tfidf_matrix = self.tfidf_vectorizer.fit_transform([document.body])
        clusters = km.predict(tfidf_matrix)
        summary = []
        for document_index in serialize_model[clusters[0]]:
            distance = self.euclidean_distance(tfidf_matrix[0], document_index['tfidf'])
            summary.append([document_index['id'], distance])
        summary.sort(key=lambda x: x[1])
        analog_ids = [item[0] for item in summary]
        return analog_ids[:count]

    def tokenize_and_stem(self, text):
        # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
        tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
        filtered_tokens = []
        # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
        for token in tokens:
            if re.search(u'[а-яА-Я\-]*', token):
                filtered_tokens.append(token)
        tokens_sw = [i for i in filtered_tokens if (i not in self.stop_words)]
        stems = [self.stemmer.stem(t) for t in tokens_sw]
        return stems

    def euclidean_distance(self, v1, v2):
        delta = v1 / sp.linalg.norm(v1.toarray()) - v2 / sp.linalg.norm(v2.toarray())
        return sp.linalg.norm(delta.toarray())
