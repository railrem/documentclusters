from peewee import *

db = PostgresqlDatabase(
    'documents',
    user = 'postgres',
    password = 'postgres',
    host = 'localhost',
)

class Document(Model):
    id = PrimaryKeyField()
    info = TextField()
    body = TextField()
    desicion = TextField()
    judge = TextField()
    title = CharField()
    path = CharField()
    all = TextField()
    class Meta:
        database = db
if __name__ == "__main__":
    Document.create_table()