from sklearn.externals import joblib

from core.model import DocumentClustering


class SerializeTool:
    PATH_KMEANS_MODEL = 'model/kmeans_model.pkl'
    PATH_KMEANS_INDEX = 'model/kmeans_index.pkl'

    PATH_SPECTRAL_MODEL = 'model/spectral_model.pkl'
    PATH_SPECTRAL_INDEX = 'model/spectral_index.pkl'

    @staticmethod
    def serializeModel(model, type_id):
        if (type_id == DocumentClustering.TYPE_KMEANS):
            path = SerializeTool.PATH_KMEANS_MODEL
        elif (type_id == DocumentClustering.TYPE_SPECTRAL):
            path = SerializeTool.PATH_SPECTRAL_MODEL
        else:
            raise Exception('unknown type')
        joblib.dump(model, path)

    @staticmethod
    def deserializeModel(type_id):
        if (type_id == DocumentClustering.TYPE_KMEANS):
            path = SerializeTool.PATH_KMEANS_MODEL
        elif (type_id == DocumentClustering.TYPE_SPECTRAL):
            path = SerializeTool.PATH_SPECTRAL_MODEL
        else:
            raise Exception('unknown type')
        serialize_model = joblib.load(path)
        return serialize_model

    @staticmethod
    def serializeClusteringIndex(model, tfidf_matrix, vectorizer, documents, type_id):
        if (type_id == DocumentClustering.TYPE_KMEANS):
            path = SerializeTool.PATH_KMEANS_INDEX
        elif (type_id == DocumentClustering.TYPE_SPECTRAL):
            path = SerializeTool.PATH_SPECTRAL_INDEX
        else:
            raise Exception('unknown type')
        clusters = model.labels_.tolist()
        cluster_index = {}
        index = 0
        for cluster in clusters:
            elem = cluster_index.get(cluster, [])
            document = {}
            document['id'] = documents[index].id
            document['tfidf'] = tfidf_matrix[index]
            elem.append(document)
            index += 1
            cluster_index[cluster] = elem
        cluster_index['vocabulary'] = vectorizer.get_feature_names()
        joblib.dump(cluster_index, path)

    @staticmethod
    def deserializeClusteringIndex(type_id):
        if (type_id == DocumentClustering.TYPE_KMEANS):
            path = SerializeTool.PATH_KMEANS_INDEX
        elif (type_id == DocumentClustering.TYPE_SPECTRAL):
            path = SerializeTool.PATH_SPECTRAL_INDEX
        else:
            raise Exception('unknown type')
        index = joblib.load(path)
        return index
