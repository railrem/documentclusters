import os
from core.db import *

class Tools:
    def fill_database(self, path):
        path = os.path.join(os.getcwd(), path)
        not_correct = 0
        for item in os.walk(path):
            base_path = item[0]
            for file in item[2]:
                document = Document()
                document.title = file
                document.path = os.path.join(base_path, file)
                f = open(document.path)
                all = ''
                desicion = ''
                body = ''
                judge = ''
                info = ''
                is_info = True
                is_body = False
                is_desicion = False
                is_judge = False
                for line in f:
                    all += line
                    line = line.lower()
                    if (line.rfind('установил') >= 0 or line.rfind('у с т а н о в и л') >= 0
                        or line.rfind('определение') >= 0 or line.rfind('о п р е д е л е н и е') >= 0):
                        is_body = True
                    if (line.rfind('определил') >= 0 or line.rfind('постановил') >= 0
                        or line.rfind('о п р е д е л и л') >= 0 or line.rfind('п о с т а н о в и л') >= 0):
                        is_desicion = True
                    if (line.rfind('судья') >= 0 or line.rfind('судьи') >= 0):
                        is_judge = True
                    if (is_info):
                        info += ' ' + line
                    if (is_body):
                        body += ' ' + line
                    if (is_desicion):
                        desicion += ' ' + line
                    if (is_judge):
                        judge += ' ' + line
                document.info = info
                document.judge = judge
                document.all = all
                document.body = body
                if (len(body) == 0):
                    not_correct += 1
                document.desicion = desicion
                print('i:%d  j:%d b:%d d:%d' % (len(info), len(judge), len(body), len(desicion)))
                document.save()
                f.close()
        print('дела не подходящие для построения модели' % not_correct)
