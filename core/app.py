import nltk
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
import re
import math
from sklearn.cluster import KMeans, SpectralClustering
from sklearn.externals import joblib
from sklearn.preprocessing import StandardScaler

from core.db import *
from sklearn.feature_extraction.text import CountVectorizer
import scipy as sp


class App:
    def __init__(self):
        self.model_path = 'model/doc_cluster.pkl'
        self.index_file = 'model/index.pkl'
        self.count_vectorizer_index = 'model/count_vectorizer.pkl'
        self.tfidf_matrix = 'model/tfidf_matrix.pkl'

        self.stemmer = SnowballStemmer("russian")
        self.tfidf_vectorizer = TfidfVectorizer(tokenizer=self.tokenize_and_stem,
                                                max_df=0.8,
                                                min_df=0.2)
        self.stop_words = nltk.corpus.stopwords.words('russian')
        self.count_vectorizer = CountVectorizer(
            tokenizer=self.tokenize_and_stem,
            stop_words=self.stop_words,
        )

    def buildModel(self,documents):
        # documents = Document.select().where(Document.body.is_null(False) & (Document.id.not_in([19366])))
        # self.tfidf_vectorizer.set_params()
        document_bodies = [document.body for document in documents]
        tfidf_matrix = self.tfidf_vectorizer.fit_transform(document_bodies)
        cluster_count = math.floor(math.log2(len(documents)))
        km = KMeans(n_clusters=cluster_count)
        km = km.fit(tfidf_matrix)
        joblib.dump(km, self.model_path)
        clusters = km.labels_.tolist()
        cluster_index = {}
        index = 0
        for cluster in clusters:
            elem = cluster_index.get(cluster, [])
            document = {}
            document['id'] = documents[index].id
            document['tfidf'] = tfidf_matrix[index]
            elem.append(document)
            index += 1
            cluster_index[cluster] = elem
        cluster_index['vocabulary'] = self.tfidf_vectorizer.get_feature_names()
        joblib.dump(cluster_index, self.index_file)

    def buildSpectralModel(self, documents):
        # documents = Document.select().where(Document.body.is_null(False) & (Document.id.not_in([19366])))
        # self.tfidf_vectorizer.set_params()
        document_bodies = [document.body for document in documents]
        tfidf_matrix = self.tfidf_vectorizer.fit_transform(document_bodies)
        cluster_count = math.floor(math.log2(len(documents)))
        km = SpectralClustering(n_clusters=cluster_count)
        km = km.fit(tfidf_matrix)
        joblib.dump(km, self.model_path)
        clusters = km.labels_.tolist()
        cluster_index = {}
        index = 0
        for cluster in clusters:
            elem = cluster_index.get(cluster, [])
            document = {}
            document['id'] = documents[index].id
            document['tfidf'] = tfidf_matrix[index]
            elem.append(document)
            index += 1
            cluster_index[cluster] = elem
        cluster_index['vocabulary'] = self.tfidf_vectorizer.get_feature_names()
        joblib.dump(cluster_index, self.index_file)

    def buildPointModel(self, documents):
        # documents = Document.select().where(Document.body.is_null(False) & (Document.id.not_in([19366])))
        # self.tfidf_vectorizer.set_params()
        document_bodies = [document.body for document in documents]
        tfidf_matrix = self.tfidf_vectorizer.fit_transform(document_bodies)
        cluster_count = math.floor(math.log2(len(documents)))
        km = StandardScaler(with_mean=True)
        km = km.fit(tfidf_matrix)
        joblib.dump(km, self.model_path)
        clusters = km.labels_.tolist()
        cluster_index = {}
        index = 0
        for cluster in clusters:
            elem = cluster_index.get(cluster, [])
            document = {}
            document['id'] = documents[index].id
            document['tfidf'] = tfidf_matrix[index]
            elem.append(document)
            index += 1
            cluster_index[cluster] = elem
        cluster_index['vocabulary'] = self.tfidf_vectorizer.get_feature_names()
        joblib.dump(cluster_index, self.index_file)

    def getAnalogs(self, document_id, count):
        document = Document.select().where(Document.id == document_id).get()
        km = joblib.load(self.model_path)
        serialize_model = joblib.load(self.index_file)
        self.tfidf_vectorizer.set_params(vocabulary=serialize_model['vocabulary'])
        tfidf_matrix = self.tfidf_vectorizer.fit_transform([document.body])
        clusters = km.predict(tfidf_matrix)
        summary = []
        for document_index in serialize_model[clusters[0]]:
            distance = self.euclidean_distance(tfidf_matrix[0], document_index['tfidf'])
            summary.append([document_index['id'], distance])
        summary.sort(key=lambda x: x[1])
        # for t in summary[0:10]:
        #     print(t)
        analog_ids = [item[0] for item in summary]
        return analog_ids[:count]

    def tokenize_and_stem(self, text):
        # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
        tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
        filtered_tokens = []
        # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
        for token in tokens:
            if re.search(u'[а-яА-Я\-]*', token):
                filtered_tokens.append(token)
        tokens_sw = [i for i in filtered_tokens if (i not in self.stop_words)]
        stems = [self.stemmer.stem(t) for t in tokens_sw]
        return stems

    def euclidean_distance(self, v1, v2):
        delta = v1 / sp.linalg.norm(v1.toarray()) - v2 / sp.linalg.norm(v2.toarray())
        return sp.linalg.norm(delta.toarray())

    def buildSimpleModel(self):
        documents = Document.select().where(Document.body.is_null(False) & (Document.id.not_in([19366])))
        texts = [document.body for document in documents]
        ids = [document.id for document in documents]
        self.count_vectorizer.set_params(max_df=0.8, min_df=0.2)
        model = self.count_vectorizer.fit_transform(texts)
        index = {}
        index['model'] = model
        index['ids'] = ids
        index['vocabulary'] = self.count_vectorizer.get_feature_names()
        joblib.dump(index, self.count_vectorizer_index)

    def getAnalogSecond(self, id, count):
        index = joblib.load(self.count_vectorizer_index)
        model = index['model']
        ids = index['ids']
        self.count_vectorizer.set_params(vocabulary=index['vocabulary'])
        n_sample, n_features = model.shape
        summary = []
        document = Document.select().where(Document.id == id).get()
        target = self.count_vectorizer.transform([document.body])
        for i in range(0, n_sample):
            distance = self.euclidean_distance(target[0], model[i])
            summary.append([ids[i], distance])
        summary.sort(key=lambda x: x[1])
        # for t in summary[0:10]:
        #     print(t)
        analog_ids = [item[0] for item in summary]
        return analog_ids
