import core.app
import time

if __name__ == "__main__":
    start_time = time.time()
    tool = core.tools.Tools()
    tool.fill_database('data_storage/acts')
    app = core.app.App()
    app.buildModel()
    finish_time = time.time() - start_time
    print('time: %s s' % round(finish_time))
