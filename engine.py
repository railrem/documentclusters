import falcon
import web
from wsgiref import simple_server
import core.app

# Configure your WSGI server to load "things.app" (app is a WSGI callable)
app = falcon.API(middleware=[
    web.json_translator.JSONTranslator(),
])

clustering_helper = core.app.App()
model = web.controllers.model_resource.ModelResource(clustering_helper)
app.add_route('/rebuild', model)

analog = web.controllers.analog_resource.AnalogsResource(clustering_helper)
app.add_route('/analogs', analog)

test = web.controllers.test_resource.TestResource(clustering_helper)
app.add_route('/test', test)

# If a responder ever raised an instance of StorageError, pass control to
# the given handler.
app.add_error_handler(web.storage_error.StorageError, web.storage_error.StorageError.handle)

# Proxy some things to another service; this example shows how you might
# send parts of an API off to a legacy system that hasn't been upgraded
# yet, or perhaps is a single cluster that all data centers have to share.

# Useful for debugging problems in your API; works with pdb.set_trace(). You
# can also use Gunicorn to host your app. Gunicorn can be configured to
# auto-restart workers when it detects a code change, and it also works
# with pdb.
if __name__ == '__main__':
    httpd = simple_server.make_server('127.0.0.1', 9005, app)
    httpd.serve_forever()
